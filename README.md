# Installation

Clone this repository into your `~/texmf/tex/latex/` folder. Every subfolder below this is optional.

For example:

    cd
    mkdir -p texmf/tex/latex/beamer
    cd texmf/tex/latex/beamer
    git clone https://bitbucket.org/Glouvel/beamerthemes.git


# Usage

In your beamer preamble, one single line is enough:

```tex
\usebeamertheme{Ys}
```

For the light theme:

```tex
\usebeamertheme[light]{Ys}
```

To add the infoline on top of each frame (section name + subsection name)

```tex
\usebeamertheme[secheader]{Ys}
```

