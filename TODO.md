- [ ] Copy and modify the `beamerouterthemesidebar.sty` to fix the following:
    - same colors for frametitle as with `infoline` (not inheriting from
      sidebar)
    - do not put an empty frametitle box when there is no title
    - add a page numbering
    - optionally add the date

